import akka.actor.{ Actor, ActorRef, Timers }
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration._
import scala.util._

object Consumer {
  case object Start
  private case object Take
  private case object Taking
}

class Consumer(buffer: ArrayBuffer[Int], producer: ActorRef) extends Actor with Timers { 
  import Consumer._

  def receive = {
    case Start => start
    case Take => take
    case _ => println("Wrong request")       
  }

  private def start = {
    if (!timers.isTimerActive(Taking)) {
      self ! Take

      // Start the taking process
      timers.startPeriodicTimer(Taking, Take, new DurationInt(Random.nextInt(10)).seconds)
    }
  }

  private def take = {
    if (buffer.size > 0) {
      println("Consumer %s takes %d" format (self.path.name, buffer.remove(0)))
    } else {
      // Stop the taking process
      println("Consumer %s waits" format self.path.name)
      timers.cancel(Taking)

      // Send a request to the producer for a new product
      producer ! Producer.Request
    }
  }
}
