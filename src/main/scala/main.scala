import akka.actor.{ ActorSystem, Props }
import scala.concurrent.duration._
import scala.collection.mutable.{ ArrayBuffer, Seq }

object Main { 
  val CustomersNumber = 2
  
  def main(args: Array[String]){
    var buffer = ArrayBuffer[Int]()

    val system = ActorSystem()
    val producer = system.actorOf(Props(classOf[Producer], buffer))
    val consumers = Seq.fill(CustomersNumber)(system.actorOf(Props(classOf[Consumer], buffer, producer)))

    producer ! Producer.Start
    consumers.foreach { _ ! Consumer.Start }
  }
}
