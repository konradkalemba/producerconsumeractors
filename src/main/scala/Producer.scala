import akka.actor.{ Actor, ActorRef, Timers }
import scala.collection.mutable.{ ArrayBuffer, Queue }
import scala.concurrent.duration._

object Producer {
  val MaxProducts = 10
  
  case object Start
  case object Request
  private case object Produce  
  private case object Production
}

class Producer(buffer: ArrayBuffer[Int]) extends Actor with Timers { 
  import Producer._
  private var id = 0
  private var consumersWaiting = new Queue[ActorRef]()

  def receive = {
    case Start => start
    case Produce => produce
    case Request => request
    case _ => println("Wrong request")       
  }

  private def start = {   
    if (!timers.isTimerActive(Production)) {
      self ! Produce

      // Start the production process
      timers.startPeriodicTimer(Production, Produce, 2 seconds)
    }
  }

  private def produce = {
    if (buffer.size < MaxProducts) {
      buffer += id
      println("Producer puts %d" format id)

      // Answer to the request sent by the consumer
      if (!consumersWaiting.isEmpty) {
        consumersWaiting.dequeue ! Consumer.Start
      }

      id += 1      
    } else {
      // Stop the production process until there is a request
      println("Producer stops")
      timers.cancel(Production)
    }
  }

  private def request = {
    // Resume production
    self ! Start

    // Add sender to queue
    consumersWaiting.enqueue(sender)
  }
}
